<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210523043016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add id for main index to wqwe_topic_read';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE wqwe_topic_read 
                ADD id 
                    INT(11) 
                    NOT NULL 
                    AUTO_INCREMENT 
                    PRIMARY KEY 
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE wqwe_topic_read 
                DROP id
        ');
    }
}
