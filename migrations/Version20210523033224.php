<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210523033224 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add id for main index to wqwe_stream_user_type';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE wqwe_stream_user_type
                ADD id 
                    INT(11) 
                    NOT NULL 
                    AUTO_INCREMENT 
                    PRIMARY KEY 
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE wqwe_stream_user_type
                DROP id
        ');
    }
}
