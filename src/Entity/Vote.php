<?php


namespace App\Entity;


use App\Helpers\DefaultsGeneratorHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 *
 * @ORM\Table(name="wqwe_vote", indexes={@ORM\Index(name="vote_ip", columns={"vote_ip"}), @ORM\Index(name="user_voter_id", columns={"user_voter_id"})})
 */
class Vote
{
    /**
     * @ORM\Column(name="target_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $targetId;

    /**
     * @ORM\Column(name="target_type", type="string", nullable=false, options={"default"="topic"}, columnDefinition="ENUM('qa_comment','qa_topic','topic','blog','user','comment')")
     */
    private string $targetType = 'topic';

    /**
     * @ORM\Column(name="vote_direction", type="int", nullable=false)
     */
    private int $voteDirection;

    /**
     * @ORM\Column(name="vote_value", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $voteValue;

    /**
     * @ORM\Column(name="vote_date", type="datetime", nullable=false)
     */
    private string $voteDate;

    /**
     * @ORM\Column(name="vote_ip", type="string", length=39, nullable=true)
     */
    private ?string $voteIp = null;

    /**
     * @ORM\Column(name="user_voter_id", type="integer", nullable=false)
     */
    private int $userVoterId;

    private function __construct(
        int $targetId,
        string $targetType,
        int $voteDirection,
        int $userVoterId
    )
    {
        $this->targetId = $targetId;
        $this->targetType = $targetType;
        $this->voteDirection = $voteDirection;
        $this->voteValue = (float) $voteDirection;
        $this->userVoterId = $userVoterId;
        $this->voteIp = '';
        $this->voteDate = DefaultsGeneratorHelper::getCurrentTime();
    }
}
