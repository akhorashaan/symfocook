<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personaltags
 *
 * @ORM\Table(name="wqwe_personaltags", indexes={@ORM\Index(name="usertag_tagname", columns={"usertag_tagname"}), @ORM\Index(name="usertag_user_id", columns={"usertag_user_id"}), @ORM\Index(name="usertag_event_type", columns={"usertag_event_type"}), @ORM\Index(name="usertag_datetime", columns={"usertag_datetime"})})
 * @ORM\Entity
 */
class Personaltags
{
    /**
     * @ORM\Column(name="usertag_id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $usertagId = null;

    /**
     * @ORM\Column(name="usertag_user_id", type="integer", nullable=false)
     */
    private int $usertagUserId;

    /**
     * @ORM\Column(name="usertag_event_type", type="integer", nullable=false)
     */
    private int $usertagEventType;

    /**
     * @ORM\Column(name="usertag_object_id", type="integer", nullable=false)
     */
    private int $usertagObjectId;

    /**
     * @ORM\Column(name="usertag_tagname", type="string", length=500, nullable=false)
     */
    private string $usertagTagname;

    /**
     * @ORM\Column(name="usertag_weight", type="integer", nullable=false)
     */
    private int $usertagWeight;

    /**
     * @ORM\Column(name="usertag_datetime", type="integer", nullable=false)
     */
    private int $usertagDatetime;

    public function __construct(
        int $usertagUserId,
        int $usertagEventType,
        int $usertagObjectId,
        string $usertagTagname,
        int $usertagWeight,
        int $usertagDatetime
    )
    {
        $this->usertagUserId = $usertagUserId;
        $this->usertagEventType = $usertagEventType;
        $this->usertagObjectId = $usertagObjectId;
        $this->usertagTagname = $usertagTagname;
        $this->usertagWeight = $usertagWeight;
        $this->usertagDatetime = $usertagDatetime;
    }

    public function getUsertagId(): ?int
    {
        return $this->usertagId;
    }

    public function getUsertagUserId(): ?int
    {
        return $this->usertagUserId;
    }

    public function getUsertagEventType(): ?int
    {
        return $this->usertagEventType;
    }

    public function getUsertagObjectId(): ?int
    {
        return $this->usertagObjectId;
    }

    public function getUsertagTagname(): ?string
    {
        return $this->usertagTagname;
    }

    public function getUsertagWeight(): ?int
    {
        return $this->usertagWeight;
    }

    public function getUsertagDatetime(): ?int
    {
        return $this->usertagDatetime;
    }
}
