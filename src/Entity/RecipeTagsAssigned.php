<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecipeTagsAssigned
 *
 * @ORM\Table(name="wqwe_recipe_tags_assigned", indexes={@ORM\Index(name="wqwe_recipe_tags_assigned_idx_recipe_tag", columns={"recipe_tag_id"})})
 * @ORM\Entity
 */
class RecipeTagsAssigned
{
    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicId;

    /**
     * @ORM\Column(name="recipe_tag_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $recipeTagId;

    public function __construct(
        int $topicId,
        int $recipeTagId
    )
    {
        $this->topicId = $topicId;
        $this->recipeTagId = $recipeTagId;
    }
}
