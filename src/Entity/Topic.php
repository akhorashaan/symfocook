<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use App\Helpers\TextHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Topic
 *
 * @ORM\Table(name="wqwe_topic", indexes={@ORM\Index(name="wqwe_topic_idx_topic_tags", columns={"topic_tags"}), @ORM\Index(name="topic_count_comment", columns={"topic_count_comment"}), @ORM\Index(name="topic_rating", columns={"topic_rating"}), @ORM\Index(name="blog_id", columns={"blog_id"}), @ORM\Index(name="wqwe_topic_idx_topic_publish_index", columns={"topic_publish_index"}), @ORM\Index(name="topic_publish", columns={"topic_publish"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="wqwe_topic_idx_topic_type", columns={"topic_type"}), @ORM\Index(name="topic_text_hash", columns={"topic_text_hash"}), @ORM\Index(name="topic_date_add", columns={"topic_date_add"})})
 */
class Topic
{
    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=true, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $topicId = null;

    /**
     * @ORM\Column(name="topic_type", type="string", length=0, nullable=false, options={"default"="topic"})
     */
    private string $topicType = 'topic';

    /**
     * @ORM\Column(name="topic_title", type="string", length=200, nullable=false)
     */
    private string $topicTitle;

    /**
     * @ORM\Column(name="topic_title_gen", type="string", length=200, nullable=true)
     */
    private ?string $topicTitleGen = null;

    /**
     * @ORM\Column(name="topic_title_abl", type="string", length=200, nullable=true)
     */
    private ?string $topicTitleAbl = null;

    /**
     * @ORM\Column(name="topic_title_acc", type="string", length=200, nullable=true)
     */
    private ?string $topicTitleAcc = null;

    /**
     * @ORM\Column(name="topic_tags", type="string", length=255, nullable=false, options={"comment"="tags separated by a comma"})
     */
    private string $topicTags;

    /**
     * @ORM\Column(name="topic_date_add", type="datetime", nullable=false)
     */
    private string $topicDateAdd;

    /**
     * @ORM\Column(name="topic_date_edit", type="datetime", nullable=true)
     */
    private ?string $topicDateEdit = null;

    /**
     * @ORM\Column(name="topic_user_ip", type="string", length=39, nullable=false)
     */
    private string $topicUserIp;

    /**
     * @ORM\Column(name="topic_publish", type="smallint", nullable=false)
     */
    private int $topicPublish = 0;

    /**
     * @ORM\Column(name="topic_publish_draft", type="smallint", nullable=false, options={"default"="1"})
     */
    private int $topicPublishDraft = 0;

    /**
     * @ORM\Column(name="topic_publish_index", type="smallint", nullable=false)
     */
    private int $topicPublishIndex = 0;

    /**
     * @ORM\Column(name="topic_rating", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $topicRating = 0.000;

    /**
     * @ORM\Column(name="topic_count_vote", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicCountVote = 0;

    /**
     * @ORM\Column(name="topic_count_vote_up", type="integer", nullable=false)
     */
    private int $topicCountVoteUp = 0;

    /**
     * @ORM\Column(name="topic_count_vote_down", type="integer", nullable=false)
     */
    private int $topicCountVoteDown = 0;

    /**
     * @ORM\Column(name="topic_count_vote_abstain", type="integer", nullable=false)
     */
    private int $topicCountVoteAbstain = 0;

    /**
     * @ORM\Column(name="topic_count_read", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicCountRead = 0;

    /**
     * @ORM\Column(name="topic_count_comment", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicCountComment = 0;

    /**
     * @ORM\Column(name="topic_count_favourite", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicCountFavourite = 0;

    /**
     * @ORM\Column(name="topic_cut_text", type="string", length=100, nullable=true)
     */
    private ?string $topicCutText = null;

    /**
     * @ORM\Column(name="topic_forbid_comment", type="smallint", nullable=false)
     */
    private int $topicForbidComment = 0;

    /**
     * @ORM\Column(name="topic_text_hash", type="string", length=32, nullable=false)
     */
    private string $topicTextHash;

    /**
     * @ORM\Column(name="topic_main_url", type="string", length=255, nullable=false)
     */
    private string $topicMainUrl;

    /**
     * @ORM\Column(name="subcategory_title", type="string", length=200, nullable=true)
     */
    private ?string $subcategoryTitle;

    /**
     * @ORM\Column(name="is_sent_to_turbo", type="integer", nullable=false)
     */
    private int $isSentToTurbo = 0;

    /**
     * @ORM\Column(name="blog_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $blogId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $userId;

    private function __construct(
        string $topicTitle,
        string $topicTags,
        string $topicUserIp,
        int $blogId,
        int $userId,
        string $subcategoryTitle
    )
    {
        $this->topicTitle = $topicTitle;
        $this->topicTags = $topicTags;
        $this->topicDateAdd = DefaultsGeneratorHelper::getCurrentTime();
        $this->topicUserIp = $topicUserIp;
        $this->topicMainUrl = TextHelper::makeSlug($topicTitle);
        $this->blogId = $blogId;
        $this->userId = $userId;
        $this->subcategoryTitle = $subcategoryTitle;
        $this->topicTextHash = DefaultsGeneratorHelper::getEmptyMd5();
    }

    public function getTopicId(): ?int
    {
        return $this->topicId;
    }

    public function getTopicType(): ?string
    {
        return $this->topicType;
    }

    public function getTopicTitle(): ?string
    {
        return $this->topicTitle;
    }

    public function getTopicTitleGen(): ?string
    {
        return $this->topicTitleGen;
    }

    public function getTopicTitleAbl(): ?string
    {
        return $this->topicTitleAbl;
    }

    public function getTopicTitleAcc(): ?string
    {
        return $this->topicTitleAcc;
    }

    public function getTopicTags(): ?string
    {
        return $this->topicTags;
    }

    public function getTopicDateAdd(): string
    {
        return $this->topicDateAdd;
    }

    public function getTopicDateEdit(): ?string
    {
        return $this->topicDateEdit;
    }

    public function getTopicUserIp(): ?string
    {
        return $this->topicUserIp;
    }

    public function getTopicPublish(): bool
    {
        return (bool) $this->topicPublish;
    }

    public function getTopicPublishDraft(): ?bool
    {
        return (bool) $this->topicPublishDraft;
    }

    public function getTopicPublishIndex(): ?bool
    {
        return (bool) $this->topicPublishIndex;
    }

    public function getTopicRating(): ?float
    {
        return $this->topicRating;
    }

    public function getTopicCountVote(): ?int
    {
        return $this->topicCountVote;
    }

    public function getTopicCountVoteUp(): ?int
    {
        return $this->topicCountVoteUp;
    }

    public function getTopicCountVoteDown(): ?int
    {
        return $this->topicCountVoteDown;
    }

    public function getTopicCountVoteAbstain(): ?int
    {
        return $this->topicCountVoteAbstain;
    }

    public function getTopicCountRead(): ?int
    {
        return $this->topicCountRead;
    }

    public function getTopicCountComment(): ?int
    {
        return $this->topicCountComment;
    }

    public function getTopicCountFavourite(): ?int
    {
        return $this->topicCountFavourite;
    }

    public function getTopicCutText(): ?string
    {
        return $this->topicCutText;
    }

    public function getTopicForbidComment(): ?bool
    {
        return (bool) $this->topicForbidComment;
    }

    public function getTopicTextHash(): ?string
    {
        return $this->topicTextHash;
    }

    public function getTopicMainUrl(): ?string
    {
        return $this->topicMainUrl;
    }

    public function getSubcategoryTitle(): ?string
    {
        return $this->subcategoryTitle;
    }

    public function getIsSentToTurbo(): ?int
    {
        return $this->isSentToTurbo;
    }
}
