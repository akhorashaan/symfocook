<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use App\Helpers\TextHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="wqwe_comment", indexes={@ORM\Index(name="comment_left", columns={"comment_left"}), @ORM\Index(name="user_type", columns={"user_id", "target_type"}), @ORM\Index(name="type_delete_publish", columns={"target_type", "comment_delete", "comment_publish"}), @ORM\Index(name="comment_pid", columns={"comment_pid"}), @ORM\Index(name="comment_right", columns={"comment_right"}), @ORM\Index(name="id_type", columns={"target_id", "target_type"}), @ORM\Index(name="type_date_rating", columns={"target_type", "comment_date", "comment_rating"}), @ORM\Index(name="comment_level", columns={"comment_level"}), @ORM\Index(name="target_parent_id", columns={"target_parent_id"}), @ORM\Index(name="IDX_93D41555A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="comment_id", type="integer", nullable=true, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $commentId = null;

    /**
     * @ORM\Column(name="comment_left", type="integer", nullable=false)
     */
    private int $commentLeft = 0;

    /**
     * @ORM\Column(name="comment_right", type="integer", nullable=false)
     */
    private int $commentRight = 0;

    /**
     * @ORM\Column(name="comment_level", type="integer", nullable=false)
     */
    private int $commentLevel = 0;

    /**
     * @ORM\Column(name="target_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $targetId = null;

    /**
     * @ORM\Column(name="target_type", type="string", length=0, nullable=false, options={"default"="topic"})
     */
    private string $targetType = 'topic';

    /**
     * @ORM\Column(name="target_parent_id", type="integer", nullable=false)
     */
    private int $targetParentId = 0;

    /**
     * @ORM\Column(name="comment_text", type="text", length=65535, nullable=false)
     */
    private string $commentText;

    /**
     * @ORM\Column(name="comment_text_hash", type="string", length=32, nullable=false)
     */
    private string $commentTextHash;

    /**
     * @ORM\Column(name="comment_date", type="datetime", nullable=false)
     */
    private string $commentDate;

    /**
     * @ORM\Column(name="comment_user_ip", type="string", length=39, nullable=false)
     */
    private string $commentUserIp;

    /**

     * @ORM\Column(name="comment_rating", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $commentRating = 0.000;

    /**
     * @ORM\Column(name="comment_count_vote", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $commentCountVote = 0;

    /**
     * @ORM\Column(name="comment_count_favourite", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $commentCountFavourite = 0;

    /**
     * @ORM\Column(name="comment_delete", type="smallint", nullable=false)
     */
    private int $commentDelete = 0;

    /**
     * @ORM\Column(name="comment_publish", type="smallint", nullable=false, options={"default"="1"})
     */
    private int $commentPublish = 1;

    /**
     * @ORM\Column(name="comment_decision", type="smallint", nullable=false)
     */
    private int $commentDecision = 0;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $user_id;

    /**
     * @ORM\Column(name="comment_pid", type="integer", nullable=false, options={"unsigned"=true})
     */
    private ?int $commentPid = 0;

    public function __construct(
        string $commentText,
        string $commentUserIp,
        int $user_id,
        int $commentPid = 0
    )
    {
        $this->commentText = $commentText;
        $this->commentTextHash = TextHelper::getHash($commentText);
        $this->commentDate = DefaultsGeneratorHelper::getCurrentTime();
        $this->commentUserIp = $commentUserIp;
        $this->user_id = $user_id;
        $this->commentPid = $commentPid;
    }

    public function getCommentId(): ?int
    {
        return $this->commentId;
    }

    public function getCommentLeft(): ?int
    {
        return $this->commentLeft;
    }

    public function getCommentRight(): ?int
    {
        return $this->commentRight;
    }

    public function getCommentLevel(): ?int
    {
        return $this->commentLevel;
    }

    public function getTargetId(): ?int
    {
        return $this->targetId;
    }


    public function getTargetType(): ?string
    {
        return $this->targetType;
    }

    public function getTargetParentId(): ?int
    {
        return $this->targetParentId;
    }

    public function getCommentText(): string
    {
        return $this->commentText;
    }

    public function getCommentTextHash(): string
    {
        return $this->commentTextHash;
    }

    public function getCommentDate(): string
    {
        return $this->commentDate;
    }

    public function getCommentUserIp(): ?string
    {
        return $this->commentUserIp;
    }

    public function getCommentRating(): ?float
    {
        return $this->commentRating;
    }

    public function getCommentCountVote(): ?int
    {
        return $this->commentCountVote;
    }

    public function getCommentCountFavourite(): ?int
    {
        return $this->commentCountFavourite;
    }

    public function getCommentDelete(): ?bool
    {
        return (bool) $this->commentDelete;
    }

    public function getCommentPublish(): ?bool
    {
        return (bool) $this->commentPublish;
    }

    public function getCommentDecision(): ?bool
    {
        return (bool) $this->commentDecision;
    }
}
