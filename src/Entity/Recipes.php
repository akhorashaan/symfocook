<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recipes
 *
 * @ORM\Table(name="wqwe_recipes", uniqueConstraints={@ORM\UniqueConstraint(name="recipe_id", columns={"recipe_id"})})
 * @ORM\Entity
 */
class Recipes
{
    /**
     * @ORM\Column(name="recipe_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $recipeId = null;

    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     */
    private int $topicId;

    /**
     * @ORM\Column(name="complexity", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $complexity = 0;

    /**
     * @ORM\Column(name="time", type="string", length=128, nullable=false)
     */
    private string $time = '';

    /**
     * @ORM\Column(name="servings", type="string", length=128, nullable=false)
     */
    private string $servings = '';

    /**
     * @ORM\Column(name="totalweight", type="string", length=63, nullable=false)
     */
    private string $totalweight = '';

    public function __construct(
        int $topicId,
    )
    {
        $this->topicId = $topicId;
    }

    public function getRecipeId(): ?int
    {
        return $this->recipeId;
    }

    public function getTopicId(): ?int
    {
        return $this->topicId;
    }

    public function getComplexity(): ?int
    {
        return $this->complexity;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function getServings(): ?string
    {
        return $this->servings;
    }

    public function getTotalweight(): ?string
    {
        return $this->totalweight;
    }
}
