<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * UserAdministrator
 *
 * @ORM\Table(name="wqwe_user_administrator", uniqueConstraints={@ORM\UniqueConstraint(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class UserAdministrator
{
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private int $userId;

    private function __construct(
        int $userId
    )
    {
        $this->userId = $userId;
    }
}
