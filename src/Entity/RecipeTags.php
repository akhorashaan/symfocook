<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecipeTags
 *
 * @ORM\Table(name="wqwe_recipe_tags", indexes={@ORM\Index(name="wqwe_recipe_tags_idx_recipe_tag_group", columns={"recipe_tag_group_id"}), @ORM\Index(name="recipe_tag_parent_id", columns={"recipe_tag_parent_id"}), @ORM\Index(name="wqwe_recipe_tags_idx_recipe_tag_title", columns={"recipe_tag_title"}), @ORM\Index(name="recipe_tag_sort", columns={"recipe_tag_sort"}), @ORM\Index(name="recipe_tag_path", columns={"recipe_tag_path"})})
 * @ORM\Entity
 */
class RecipeTags
{
    /**
     * @ORM\Column(name="recipe_tag_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $recipeTagId = null;

    /**
     * @ORM\Column(name="recipe_tag_parent_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $recipeTagParentId = null;

    /**
     * @ORM\Column(name="recipe_tag_path", type="string", length=31, nullable=false)
     */
    private string $recipeTagPath = '';

    /**
     * @ORM\Column(name="recipe_tag_group_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $recipeTagGroupId = null;

    /**
     * @ORM\Column(name="recipe_tag_title", type="string", length=250, nullable=true)
     */
    private ?string $recipeTagTitle = null;

    /**
     * @ORM\Column(name="recipe_tag_title_gen", type="string", length=255, nullable=true)
     */
    private ?string $recipeTagTitleGen = null;

    /**
     * @ORM\Column(name="recipe_tag_title_abl", type="string", length=255, nullable=true)
     */
    private ?string $recipeTagTitleAbl = null;

    /**
     * @ORM\Column(name="recipe_tag_title_acc", type="string", length=255, nullable=true)
     */
    private ?string $recipeTagTitleAcc = null;

    /**
     * @ORM\Column(name="recipe_tag_sort", type="integer", nullable=true, options={"unsigned"=true})
     */
    private int|null $recipeTagSort = 0;

    /**
     * @ORM\Column(name="wiki_text", type="string", length=5000, nullable=true)
     */
    private ?string $wikiText = null;

    /**
     * @ORM\Column(name="topic_count", type="integer", nullable=false)
     */
    private int $topicCount = 0;

    /**
     * @ORM\Column(name="description_new", type="text", length=65535, nullable=true)
     */
    private ?string $descriptionNew = null;

    /**
     * @ORM\Column(name="description_video", type="text", length=65535, nullable=true)
     */
    private ?string $descriptionVideo = null;

    public function getRecipeTagId(): ?int
    {
        return $this->recipeTagId;
    }

    public function getRecipeTagParentId(): ?int
    {
        return $this->recipeTagParentId;
    }

    public function getRecipeTagPath(): ?string
    {
        return $this->recipeTagPath;
    }

    public function getRecipeTagGroupId(): ?int
    {
        return $this->recipeTagGroupId;
    }

    public function getRecipeTagTitle(): ?string
    {
        return $this->recipeTagTitle;
    }

    public function getRecipeTagTitleGen(): ?string
    {
        return $this->recipeTagTitleGen;
    }

    public function getRecipeTagTitleAbl(): ?string
    {
        return $this->recipeTagTitleAbl;
    }

    public function getRecipeTagTitleAcc(): ?string
    {
        return $this->recipeTagTitleAcc;
    }

    public function getWikiText(): ?string
    {
        return $this->wikiText;
    }

    public function getDescriptionNew(): ?string
    {
        return $this->descriptionNew;
    }

    public function getDescriptionVideo(): ?string
    {
        return $this->descriptionVideo;
    }
}
