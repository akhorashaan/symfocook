<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TopicContent
 *
 * @ORM\Table(name="wqwe_topic_content")
 * @ORM\Entity
 */
class TopicContent
{
    /**
     * @ORM\Column(name="topic_text", type="text", length=0, nullable=false)
     */
    private string $topicText;

    /**
     * @ORM\Column(name="topic_text_short", type="text", length=65535, nullable=false)
     */
    private string $topicTextShort;

    /**
     * @ORM\Column(name="topic_text_source", type="text", length=0, nullable=false)
     */
    private string $topicTextSource;

    /**
     * @ORM\Column(name="topic_extra", type="text", length=65535, nullable=false)
     */
    private string $topicExtra;

    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $topicId;

    private function __construct(
        string $topicText,
        string $topicTextShort,
        string $topicTextSource,
        string $topicExtra,
        int $topicId
    )
    {
        #TODO: Понять, как генерируются короткие описания
        $this->topicText = $topicText;
        $this->topicTextShort = $topicTextShort;
        $this->topicTextSource = $topicTextSource;
        $this->topicExtra = $topicExtra;
        $this->topicId = $topicId;
    }

    public function getTopicText(): string
    {
        return $this->topicText;
    }

    public function getTopicTextShort(): string
    {
        return $this->topicTextShort;
    }

    public function getTopicTextSource(): string
    {
        return $this->topicTextSource;
    }

    public function getTopicExtra(): string
    {
        return $this->topicExtra;
    }
}
