<?php


namespace App\Entity;


use App\Helpers\DefaultsGeneratorHelper;
use App\Helpers\UserHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="wqwe_user", uniqueConstraints={@ORM\UniqueConstraint(name="user_login", columns={"user_login"}), @ORM\UniqueConstraint(name="user_mail", columns={"user_mail"})}, indexes={@ORM\Index(name="user_activate", columns={"user_activate"}), @ORM\Index(name="user_rating", columns={"user_rating"}), @ORM\Index(name="user_profile_sex", columns={"user_profile_sex"}), @ORM\Index(name="user_activate_key", columns={"user_activate_key"})})
 */
class User
{
    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $userId = null;

    /**
     * @ORM\Column(name="user_login", type="string", length=30, nullable=false)
     */
    private string $userLogin;

    /**
     * @ORM\Column(name="user_password", type="string", length=50, nullable=false)
     */
    private string $userPassword;

    /**
     * @ORM\Column(name="user_mail", type="string", length=50, nullable=false)
     */
    private string $userMail;

    /**
     * @ORM\Column(name="user_skill", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $userSkill = 0.000;

    /**
     * @ORM\Column(name="user_date_register", type="datetime", nullable=false)
     */
    private string $userDateRegister;

    /**
     * @ORM\Column(name="user_date_activate", type="datetime", nullable=true)
     */
    private ?string $userDateActivate = null;

    /**
     * @ORM\Column(name="user_date_comment_last", type="datetime", nullable=true)
     */
    private ?string $userDateCommentLast = null;

    /**
     * @ORM\Column(name="user_ip_register", type="string", length=39, nullable=false)
     */
    private string $userIpRegister;

    /**
     * @ORM\Column(name="user_rating", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $userRating = 0.000;

    /**
     * @ORM\Column(name="user_count_vote", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $userCountVote = 0;

    /**
     * @ORM\Column(name="user_activate", type="integer", nullable=false)
     */
    private int $userActivate;

    /**
     * @ORM\Column(name="user_activate_key", type="string", length=32, nullable=true)
     */
    private ?string $userActivateKey = null;

    /**
     * @ORM\Column(name="user_profile_name", type="string", length=50, nullable=true)
     */
    private ?string $userProfileName = null;

    /**
     * @ORM\Column(name="user_profile_name_gen", type="string", length=50, nullable=true)
     */
    private ?string $userProfileNameGen = null;

    /**
     * @ORM\Column(name="user_profile_name_abl", type="string", length=50, nullable=true)
     */
    private ?string $userProfileNameAbl = null;

    /**
     * @ORM\Column(name="user_profile_sex", length=0, nullable=false, options={"default"="other"}, columnDefinition="ENUM('man','woman','other')")
     */
    private string $userProfileSex = 'other';

    /**
     * @ORM\Column(name="user_profile_country", type="string", length=30, nullable=true)
     */
    private ?string $userProfileCountry = null;

    /**
     * @ORM\Column(name="user_profile_region", type="string", length=30, nullable=true)
     */
    private ?string $userProfileRegion = null;

    /**
     * @ORM\Column(name="user_profile_city", type="string", length=30, nullable=true)
     */
    private ?string $userProfileCity = null;

    /**
     * @ORM\Column(name="user_profile_birthday", type="datetime", nullable=true)
     */
    private ?string $userProfileBirthday = null;

    /**
     * @ORM\Column(name="user_profile_site", type="string", length=200, nullable=true)
     */
    private ?string $userProfileSite = null;

    /**
     * @ORM\Column(name="user_profile_site_name", type="string", length=50, nullable=true)
     */
    private ?string $userProfileSiteName = null;

    /**
     * @ORM\Column(name="user_profile_icq", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private ?int $userProfileIcq = null;

    /**
     * @ORM\Column(name="user_profile_about", type="text", length=65535, nullable=true)
     */
    private ?string $userProfileAbout = null;

    /**
     * @ORM\Column(name="user_profile_date", type="datetime", nullable=true)
     */
    private ?string $userProfileDate = null;

    /**
     * @ORM\Column(name="user_profile_avatar", type="string", length=250, nullable=true)
     */
    private ?string $userProfileAvatar = null;

    /**
     * @ORM\Column(name="user_profile_foto", type="string", length=250, nullable=true)
     */
    private ?string $userProfileFoto = null;

    /**
     * @ORM\Column(name="user_settings_notice_new_topic", type="integer", nullable=false, options={"default"="1"})
     */
    private int $userSettingsNoticeNewTopic = 1;

    /**
     * @ORM\Column(name="user_settings_notice_new_comment", type="integer", nullable=false, options={"default"="1"})
     */
    private int $userSettingsNoticeNewComment = 1;

    /**
     * @ORM\Column(name="user_settings_notice_new_talk", type="integer", nullable=false, options={"default"="1"})
     */
    private int $userSettingsNoticeNewTalk = 1;

    /**
     * @ORM\Column(name="user_settings_notice_reply_comment", type="integer", nullable=false, options={"default"="1"})
     */
    private int $userSettingsNoticeReplyComment = 1;

    /**
     * @ORM\Column(name="user_settings_notice_new_friend", type="integer", nullable=false, options={"default"="1"})
     */
    private int $userSettingsNoticeNewFriend = 1;

    /**
     * @ORM\Column(name="user_settings_timezone", type="string", length=6, nullable=true)
     */
    private ?string $userSettingsTimezone = null;

    private const USER_ACTIVATED = 1;
    private const USER_INACTIVE = 0;

    public function __construct(
        string $userLogin,
        string $userPassword,
        string $userMail,
        string $userIpRegister,
        int $userActivate
    )
    {
        $this->userLogin = $userLogin;
        $this->userPassword = UserHelper::hashPassword($userPassword);
        $this->userMail = $userMail;
        $this->userIpRegister = $userIpRegister;
        $this->userActivate = $userActivate ? self::USER_ACTIVATED : self::USER_INACTIVE;
        $this->userDateRegister = DefaultsGeneratorHelper::getCurrentTime();
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getUserLogin(): string
    {
        return $this->userLogin;
    }

    public function getUserPassword(): string
    {
        return $this->userPassword;
    }

    public function getUserMail(): string
    {
        return $this->userMail;
    }

    public function getUserDateRegister(): string
    {
        return $this->userDateRegister;
    }

    public function getUserIpRegister(): string
    {
        return $this->userIpRegister;
    }

    public function getUserActivate(): bool
    {
        return (bool)$this->userActivate;
    }

    public function getUserActivateKey(): ?string
    {
        return $this->userActivateKey;
    }

    public function getUserProfileName(): ?string
    {
        return $this->userProfileName;
    }

    public function getUserProfileSex(): ?string
    {
        return $this->userProfileSex;
    }

    public function getUserProfileBirthday(): ?string
    {
        return $this->userProfileBirthday;
    }

    public function getUserProfileSite(): ?string
    {
        return $this->userProfileSite;
    }

    public function getUserProfileSiteName(): ?string
    {
        return $this->userProfileSiteName;
    }

    public function getUserProfileAbout(): ?string
    {
        return $this->userProfileAbout;
    }

    public function getUserProfileDate(): ?string
    {
        return $this->userProfileDate;
    }

    public function getUserProfileAvatar(): ?string
    {
        return $this->userProfileAvatar;
    }
}
