<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use Doctrine\ORM\Mapping as ORM;

#TODO Maybe not need
/**
 * Session
 *
 * @ORM\Table(name="wqwe_session", uniqueConstraints={@ORM\UniqueConstraint(name="user_id", columns={"user_id"})}, indexes={@ORM\Index(name="session_date_last", columns={"session_date_last"})})
 * @ORM\Entity
 */
class Session
{
    /**
     * @ORM\Column(name="session_key", type="string", length=32, nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?string $sessionKey = null;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $userId;

    /**
     * @ORM\Column(name="session_ip_create", type="string", length=39, nullable=false)
     */
    private string $sessionIpCreate;

    /**
     * @ORM\Column(name="session_ip_last", type="string", length=39, nullable=false)
     */
    private string $sessionIpLast;

    /**
     * @ORM\Column(name="session_date_create", type="datetime", nullable=true)
     */
    private ?string $sessionDateCreate = null;

    /**
     * @ORM\Column(name="session_date_last", type="datetime", nullable=false)
     */
    private string $sessionDateLast;

    public function __construct(
        int $userId,
        string $sessionIpCreate,
        string $sessionIpLast
    )
    {
        $this->userId = $userId;
        $this->sessionIpCreate = $sessionIpCreate;
        $this->sessionIpLast = $sessionIpLast;
        $this->sessionDateLast = DefaultsGeneratorHelper::getCurrentTime();
    }

    public function getSessionKey(): ?string
    {
        return $this->sessionKey;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function getSessionIpCreate(): ?string
    {
        return $this->sessionIpCreate;
    }

    public function getSessionIpLast(): ?string
    {
        return $this->sessionIpLast;
    }

    public function getSessionDateCreate(): ?string
    {
        return $this->sessionDateCreate;
    }

    public function getSessionDateLast(): ?string
    {
        return $this->sessionDateLast;
    }
}
