<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TopicPhoto
 *
 * @ORM\Table(name="wqwe_topic_photo", indexes={@ORM\Index(name="topic_id", columns={"topic_id"}), @ORM\Index(name="target_tmp", columns={"target_tmp"})})
 * @ORM\Entity
 */
class TopicPhoto
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private string $path;

    /**
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(name="target_tmp", type="string", length=40, nullable=true)
     */
    private ?string $targetTmp = null;

    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicId;

    private function __construct(
        string $path, int $topicId
    )
    {
        $this->path = $path;
        $this->topicId = $topicId;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
