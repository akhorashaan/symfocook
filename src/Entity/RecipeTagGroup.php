<?php

namespace App\Entity;

use App\Helpers\TextHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * RecipeTagGroup
 *
 * @ORM\Table(name="wqwe_recipe_tag_group", indexes={@ORM\Index(name="recipe_tag_sort", columns={"recipe_tag_sort"}), @ORM\Index(name="wqwe_recipe_tag_group_idx_recipe_tag_group_slug", columns={"recipe_tag_group_slug"})})
 * @ORM\Entity
 */
class RecipeTagGroup
{
    /**
     * @ORM\Column(name="recipe_tag_group_id", type="integer", nullable=true, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $recipeTagGroupId = null;

    /**
     * @ORM\Column(name="recipe_tag_group_title", type="string", length=250, nullable=true)
     */
    private string $recipeTagGroupTitle;

    /**
     * @ORM\Column(name="recipe_tag_group_slug", type="string", length=250, nullable=true)
     */
    private string $recipeTagGroupSlug;

    /**
     * @ORM\Column(name="recipe_tag_sort", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $recipeTagSort = 0;

    public function __construct(
        string $recipeTagGroupTitle,
    )
    {
        $this->recipeTagGroupTitle = $recipeTagGroupTitle;
        $this->recipeTagGroupSlug = TextHelper::makeSlug($recipeTagGroupTitle);
    }

    public function getRecipeTagGroupId(): ?int
    {
        return $this->recipeTagGroupId;
    }

    public function getRecipeTagGroupTitle(): ?string
    {
        return $this->recipeTagGroupTitle;
    }

    public function getRecipeTagGroupSlug(): ?string
    {
        return $this->recipeTagGroupSlug;
    }
}
