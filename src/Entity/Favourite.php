<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Favourite
 *
 * @ORM\Table(name="wqwe_favourite", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_target_id_type", columns={"user_id", "target_id", "target_type"})}, indexes={@ORM\Index(name="id_type", columns={"target_id", "target_type"}), @ORM\Index(name="target_publish", columns={"target_publish"}), @ORM\Index(name="IDX_849E9430A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Favourite
{
    /**
     * @ORM\Column(name="target_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private ?int $targetId = null;

    /**
     * @ORM\Column(name="target_type", type="string", length=0, nullable=false, options={"default"="topic"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $targetType = 'topic';

    /**
     * @ORM\Column(name="target_publish", type="boolean", nullable=true, options={"default"="1"})
     */
    private ?bool $targetPublish = true;

    /**
     * @ORM\Column(name="tags", type="string", length=250, nullable=false)
     */
    private string $tags = '';

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private string $created;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $user_id;

    public function __construct(
        int $targetId,
        int $user_id
    )
    {
        $this->targetId = $targetId;
        $this->created = DefaultsGeneratorHelper::getCurrentTime();
        $this->user_id = $user_id;
    }

    public function getTargetId(): ?int
    {
        return $this->targetId;
    }

    public function getTargetType(): ?string
    {
        return $this->targetType;
    }

    public function getTargetPublish(): ?bool
    {
        return $this->targetPublish;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function getCreated(): string
    {
        return $this->created;
    }
}
