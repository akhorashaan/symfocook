<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table(name="wqwe_blog", indexes={@ORM\Index(name="blog_title", columns={"blog_title"}), @ORM\Index(name="user_owner_id", columns={"user_owner_id"}), @ORM\Index(name="blog_count_topic", columns={"blog_count_topic"}), @ORM\Index(name="blog_type", columns={"blog_type"}), @ORM\Index(name="blog_url", columns={"blog_url"})})
 */
class Blog
{
    /**
     * @ORM\Column(name="blog_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $blogId = null;

    /**
     * @ORM\Column(name="blog_title", type="string", length=200, nullable=false)
     */
    private string $blogTitle;

    /**
     * @ORM\Column(name="blog_title_gen", type="string", length=255, nullable=true)
     */
    private ?string $blogTitleGen = null;

    /**
     * @ORM\Column(name="blog_title_abl", type="string", length=255, nullable=true)
     */
    private ?string $blogTitleAbl = null;

    /**
     * @ORM\Column(name="blog_title_acc", type="string", length=255, nullable=true)
     */
    private ?string $blogTitleAcc = null;

    /**
     * @ORM\Column(name="blog_description", type="text", length=65535, nullable=false)
     */
    private string $blogDescription;

    /**
     * @ORM\Column(name="blog_type", type="string", length=36, nullable=true, options={"default"="personal"})
     */
    private string $blogType = 'personal';

    /**
     * @ORM\Column(name="blog_date_add", type="datetime", nullable=false)
     */
    private string $blogDateAdd;

    /**
     * @ORM\Column(name="blog_date_edit", type="datetime", nullable=true)
     */
    private ?string $blogDateEdit = null;

    /**
     * @ORM\Column(name="blog_rating", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $blogRating = 0.000;

    /**
     * @ORM\Column(name="blog_count_vote", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $blogCountVote = 0;

    /**
     * @ORM\Column(name="blog_count_user", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $blogCountUser = 0;

    /**
     * @ORM\Column(name="blog_count_topic", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $blogCountTopic = 0;

    /**
     * @ORM\Column(name="blog_limit_rating_topic", type="float", precision=9, scale=3, nullable=false, options={"default"="0.000"})
     */
    private float $blogLimitRatingTopic = 0.000;

    /**
     * @ORM\Column(name="blog_url", type="string", length=200, nullable=true)
     */
    private ?string $blogUrl = null;

    /**
     * @ORM\Column(name="blog_avatar", type="string", length=250, nullable=true)
     */
    private ?string $blogAvatar = null;

    /**
     * @ORM\Column(name="topic_count", type="integer", nullable=false)
     */
    private int $topicCount = 0;

    /**
     * @ORM\Column(name="user_owner", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $userOwner;

    public function __construct(
        string $blogTitle,
        string $blogDescription,
        int $userOwner
    )
    {
        $this->blogTitle = $blogTitle;
        $this->blogDescription = $blogDescription;
        $this->blogDateAdd = DefaultsGeneratorHelper::getCurrentTime();
        $this->userOwner = $userOwner;
    }

    public function getBlogId(): ?int
    {
        return $this->blogId;
    }

    public function getBlogTitle(): ?string
    {
        return $this->blogTitle;
    }

    public function getBlogTitleGen(): ?string
    {
        return $this->blogTitleGen;
    }


    public function getBlogTitleAbl(): ?string
    {
        return $this->blogTitleAbl;
    }

    public function getBlogTitleAcc(): ?string
    {
        return $this->blogTitleAcc;
    }

    public function getBlogDescription(): ?string
    {
        return $this->blogDescription;
    }

    public function getBlogType(): ?string
    {
        return $this->blogType;
    }

    public function getBlogDateAdd(): string
    {
        return $this->blogDateAdd;
    }

    public function getBlogDateEdit(): ?string
    {
        return $this->blogDateEdit;
    }

    public function getBlogRating(): ?float
    {
        return $this->blogRating;
    }

    public function getBlogCountVote(): ?int
    {
        return $this->blogCountVote;
    }

    public function getBlogCountUser(): ?int
    {
        return $this->blogCountUser;
    }

    public function getBlogCountTopic(): ?int
    {
        return $this->blogCountTopic;
    }

    public function getBlogLimitRatingTopic(): ?float
    {
        return $this->blogLimitRatingTopic;
    }

    public function getBlogUrl(): ?string
    {
        return $this->blogUrl;
    }

    public function getBlogAvatar(): ?string
    {
        return $this->blogAvatar;
    }

    public function getTopicCount(): ?int
    {
        return $this->topicCount;
    }
}
