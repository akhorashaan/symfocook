<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TopicTag
 *
 * @ORM\Table(name="wqwe_topic_tag", indexes={@ORM\Index(name="topic_tag_text", columns={"topic_tag_text"}), @ORM\Index(name="topic_id", columns={"topic_id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="blog_id", columns={"blog_id"})})
 * @ORM\Entity
 */
class TopicTag
{
    /**
     * @ORM\Column(name="topic_tag_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $topicTagId = null;

    /**
     * @ORM\Column(name="topic_tag_text", type="string", length=50, nullable=false)
     */
    private string $topicTagText;

    /**
    /**
     * @ORM\Column(name="topic_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $topicId;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $userId;

    /**
     * @ORM\Column(name="blog_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private int $blogId;

    private function __construct(
        string $topicTagText,
        int $topicId,
        int $userId,
        int $blogId
    )
    {
        $this->topicTagText = $topicTagText;
        $this->topicId = $topicId;
        $this->userId = $userId;
        $this->blogId = $blogId;
    }

    public function getTopicTagText(): ?string
    {
        return $this->topicTagText;
    }
}
