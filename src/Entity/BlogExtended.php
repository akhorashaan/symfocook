<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogExtended
 *
 * @ORM\Table(name="wqwe_mblog_extended", indexes={@ORM\Index(name="wqwe_mblog_extended_idx_category", columns={"category_id"})})
 * @ORM\Entity
 */
class BlogExtended
{
    /**
     * @ORM\Column(name="blog_id", type="integer", nullable=false)
     */
    private int $blogId;

    /**
     * @ORM\Column(name="blog_premoderation", type="smallint", nullable=true)
     */
    private int $blogPremoderation = 0;

    /**
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     */
    private int $categoryId;

    public function __construct(
        int $blogId,
        int $categoryId
    )
    {
        $this->blogId = $blogId;
        $this->categoryId = $categoryId;
    }

    public function getBlogId(): int
    {
        return $this->blogId;
    }

    public function getCategoryId(): int
    {
        return $this->categoryId;
    }
}
