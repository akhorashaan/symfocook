<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use App\Helpers\TextHelper;
use Doctrine\ORM\Mapping as ORM;

/**
 * BlogCategory
 *
 * @ORM\Table(name="wqwe_mblog_category", indexes={@ORM\Index(name="category_title", columns={"category_title"}), @ORM\Index(name="category_url", columns={"category_url"})})
 * @ORM\Entity
 */
class BlogCategory
{
    /**
     * @ORM\Column(name="category_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $categoryId = null;

    /**
     * @ORM\Column(name="category_title", type="string", length=100, nullable=false)
     */
    private string $categoryTitle;

    /**
     * @ORM\Column(name="category_url", type="string", length=100, nullable=false)
     */
    private string $categoryUrl;

    /**
     * @ORM\Column(name="category_description", type="text", length=65535, nullable=false)
     */
    private string $categoryDescription;

    /**
     * @ORM\Column(name="category_date_add", type="datetime", nullable=true)
     */
    private ?string $categoryDateAdd = null;

    /**
     * @ORM\Column(name="category_date_edit", type="datetime", nullable=true)
     */
    private ?string $categoryDateEdit = null;

    /**
     * @ORM\Column(name="category_owner_id", type="bigint", nullable=true)
     */
    private ?int $categoryOwnerId = null;

    /**
     * @ORM\Column(name="category_parent_id", type="bigint", nullable=true)
     */
    private ?int $categoryParentId = 0;

    /**
     * @ORM\Column(name="category_title_path", type="string", length=250, nullable=true)
     */
    private ?string $categoryTitlePath = null;

    /**
     * @ORM\Column(name="topic_count", type="integer", nullable=false)
     */
    private int $topicCount = 0;

    /**
     * @ORM\Column(name="category_description_new", type="text", length=65535, nullable=true)
     */
    private ?string $categoryDescriptionNew = null;

    public function __construct(
        string $categoryTitle,
        string $categoryDescription
    )
    {
        $this->categoryTitle = $categoryTitle;
        $this->categoryUrl = TextHelper::makeSlug($categoryTitle);
        $this->categoryDescription = $categoryDescription;
        $this->categoryDateAdd = DefaultsGeneratorHelper::getCurrentTime();
        $this->categoryTitlePath = $categoryTitle;
    }

    public function getCategoryId(): ?int
    {
        return $this->categoryId;
    }

    public function getCategoryTitle(): ?string
    {
        return $this->categoryTitle;
    }

    public function getCategoryUrl(): ?string
    {
        return $this->categoryUrl;
    }

    public function getCategoryDescription(): ?string
    {
        return $this->categoryDescription;
    }

    public function getCategoryOwnerId(): ?int
    {
        return $this->categoryOwnerId;
    }

    public function getTopicCount(): ?int
    {
        return $this->topicCount;
    }

    public function getCategoryDescriptionNew(): ?string
    {
        return $this->categoryDescriptionNew;
    }
}
