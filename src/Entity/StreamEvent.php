<?php

namespace App\Entity;

use App\Helpers\DefaultsGeneratorHelper;
use Doctrine\ORM\Mapping as ORM;

#TODO: Maybe not need
/**
 * StreamEvent
 *
 * @ORM\Table(name="wqwe_stream_event", indexes={@ORM\Index(name="publish", columns={"publish"}), @ORM\Index(name="event_type", columns={"event_type", "user_id"}), @ORM\Index(name="target_id", columns={"target_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class StreamEvent
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="event_type", type="string", length=100, nullable=false)
     */
    private string $eventType;

    /**
     * @ORM\Column(name="target_id", type="integer", nullable=false)
     */
    private ?int $targetId;

    /**
     * @ORM\Column(name="date_added", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private string $dateAdded;

    /**
     * @ORM\Column(name="publish", type="smallint", nullable=false, options={"default"="1"})
     */
    private int $publish = 1;

    /**
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private int $userId;

    public function __construct(
        string $eventType,
        int $targetId,
        int $userId
    )
    {
        $this->eventType = $eventType;
        $this->targetId = $targetId;
        $this->dateAdded = DefaultsGeneratorHelper::getCurrentTime();
        $this->userId = $userId;
    }
}
