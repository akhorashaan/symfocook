<?php


namespace App\Helpers;


class DefaultsGeneratorHelper
{
    public static function getCurrentTime(): string
    {
        return date('Y-m-d H:i:s');
    }

    public static function getEmptyMd5(): string
    {
        return md5('`');
    }
}
